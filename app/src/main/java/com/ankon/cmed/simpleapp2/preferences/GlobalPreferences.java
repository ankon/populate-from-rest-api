package com.ankon.cmed.simpleapp2.preferences;

/**
 * Created by tasnimankonmanzur on 11/11/17.
 */

public class GlobalPreferences {
    public static final String BASE_URL = "https://randomuser.me/api/";
    public static final String PORTRAITS_MEN = "portraits/men/";
    public static final String PORTRAITS_WOMEN = "portraits/women/";
    public static final String DATA_URL = "http://dropbox.sandbox2000.com/intrvw/users.json";

    public GlobalPreferences() {

    }
}
