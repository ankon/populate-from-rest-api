package com.ankon.cmed.simpleapp2.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ankon.cmed.simpleapp2.R;
import com.ankon.cmed.simpleapp2.customclass.PeopleClass;
import com.ankon.cmed.simpleapp2.dataadapter.PeopleAdapter;
import com.ankon.cmed.simpleapp2.listadapter.PeopleList;
import com.ankon.cmed.simpleapp2.methods.GlobalMethods;
import com.ankon.cmed.simpleapp2.preferences.GlobalPreferences;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    Context context;

    ListView listView;

    //General Progress Dialog
    ProgressDialog progressDialog;
    boolean progressDialogShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        progressDialogShow = false;

        initializeOnScreenElements();
    }

    private void initializeOnScreenElements() {
        listView = (ListView) findViewById(R.id.lv_people);

        new PeopleAsyncTask().execute(GlobalPreferences.DATA_URL);

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Checking Credentials");
        progressDialog.setMessage("Please wait a while");
        progressDialog.setCancelable(false);
        progressDialog.show();
        progressDialogShow = true;
    }

    private void populateData(JSONArray users) throws JSONException {
        PeopleClass[] peoples = new PeopleClass[users.length()];
        PeopleAdapter peopleAdapter = new PeopleAdapter(context);

        for (int i = 0; i < users.length(); i++) {
            peoples[i] = peopleAdapter.loadData(users.getJSONObject(i));
        }

        listView.setAdapter(new PeopleList(context, android.R.layout.simple_list_item_1, R.id.first_name, peoples));

        peopleAdapter = null;
    }

    private void CloseDialog() {
        if (progressDialogShow) {
            progressDialogShow = false;
            progressDialog.dismiss();
        }
    }

    private class PeopleAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String result = "";

            try {
                Log.d("URL", params[0]);
                URL url = new URL(params[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setReadTimeout(20000);
                conn.setConnectTimeout(30000);

                int responseCode = conn.getResponseCode();

                Log.d("RESPONSE CODE", String.valueOf(responseCode) + " " + HttpsURLConnection.HTTP_OK);

                if(responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while((line = reader.readLine()) != null) {
                        result += line;
                    }
                }
                else {
                    result = "";
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (!s.isEmpty()) {
                try {
                    JSONObject result = new JSONObject(s);

                    if (!result.isNull("users")) {
                        JSONArray users = result.getJSONArray("users");
                        if (GlobalMethods.isForeground(context)) {
                            if (users.length() > 0) {
                                CloseDialog();
                                populateData(users);
                            }
                        }
                    } else  {
                        if (GlobalMethods.isForeground(context)) {
                            CloseDialog();
                            Toast.makeText(getApplicationContext(), "Could not fetch data", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    if (GlobalMethods.isForeground(context)) {
                        CloseDialog();
                        Toast.makeText(getApplicationContext(), "Could not fetch data", Toast.LENGTH_SHORT).show();
                    }
                    e.printStackTrace();
                }
            } else {
                if (GlobalMethods.isForeground(context))
                    CloseDialog();
                Toast.makeText(getApplicationContext(), "Could not fetch data", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CloseDialog();
    }
}
