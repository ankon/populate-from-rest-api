package com.ankon.cmed.simpleapp2.customclass;

/**
 * Created by tasnimankonmanzur on 10/31/17.
 */

public class PeopleClass {
    public long id;
    public String firstName, lastName, mobileNumber, gender;
    public int photo;

    public PeopleClass(long id, String firstName, String lastName, String mobileNumber, String gender, int photo) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
        this.gender = gender;
        this.photo = photo;
    }
}
