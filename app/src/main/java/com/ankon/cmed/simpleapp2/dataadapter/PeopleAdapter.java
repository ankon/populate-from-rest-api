package com.ankon.cmed.simpleapp2.dataadapter;

import android.content.Context;

import com.ankon.cmed.simpleapp2.customclass.PeopleClass;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tasnimankonmanzur on 11/11/17.
 */

public class PeopleAdapter {
    Context context;

    public PeopleAdapter(Context context) {
        this.context = context;
    }

    public PeopleClass loadData(JSONObject jsonObject) throws JSONException {
        return new PeopleClass(jsonObject.getLong("id"),
                jsonObject.getString("firstName"),
                jsonObject.getString("lastName"),
                jsonObject.getJSONObject("phones").getString("mobile"),
                jsonObject.getString("gender"),
                jsonObject.getInt("photo"));
    }
}
