package com.ankon.cmed.simpleapp2.listadapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ankon.cmed.simpleapp2.R;
import com.ankon.cmed.simpleapp2.customclass.PeopleClass;
import com.ankon.cmed.simpleapp2.preferences.GlobalPreferences;
import com.squareup.picasso.Picasso;

/**
 * Created by tasnimankonmanzur on 11/11/17.
 */

public class PeopleList extends ArrayAdapter<PeopleClass> {
    private Context context;
    private PeopleClass[] objects;

    public PeopleList(@NonNull Context context, int resource, int textViewResourceId, @NonNull PeopleClass[] objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("ViewHolder") View row = inflater.inflate(R.layout.list_item_people, parent, false);

        TextView firstName = (TextView) row.findViewById(R.id.first_name);
        TextView lastName = (TextView) row.findViewById(R.id.last_name);
        TextView mobileNumber = (TextView) row.findViewById(R.id.mobile_number);
        ImageView profileImage = (ImageView) row.findViewById(R.id.profile_image);

        firstName.setText(objects[position].firstName);
        lastName.setText(objects[position].lastName);
        mobileNumber.setText(objects[position].mobileNumber);

        if (objects[position].gender.equals("male")) {
            Picasso.with(context).load(GlobalPreferences.BASE_URL +
                    GlobalPreferences.PORTRAITS_MEN +
                    objects[position].photo + ".jpg").into(profileImage);
        } else if (objects[position].gender.equals("female")) {
            Picasso.with(context).load(GlobalPreferences.BASE_URL +
                    GlobalPreferences.PORTRAITS_WOMEN +
                    objects[position].photo + ".jpg").into(profileImage);
        }

        return row;
    }
}
