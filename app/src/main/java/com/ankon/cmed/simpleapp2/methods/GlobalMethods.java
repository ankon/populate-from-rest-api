package com.ankon.cmed.simpleapp2.methods;

import android.app.ActivityManager;
import android.content.Context;

import java.util.List;

/**
 * Created by tasnimankonmanzur on 11/11/17.
 */

public class GlobalMethods {
    public GlobalMethods() {}

    public static boolean isForeground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        assert am != null;
        List<ActivityManager.RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : tasks) {
            if (ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND == appProcess.importance && packageName.equals(appProcess.processName)) {
                return true;
            }
        }
        return false;
    }
}
